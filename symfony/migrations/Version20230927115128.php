<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230927115128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE music (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE origin (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, origin_id INT DEFAULT NULL, country_id INT DEFAULT NULL, music_id INT DEFAULT NULL, group_name VARCHAR(255) DEFAULT NULL, start_year VARCHAR(255) DEFAULT NULL, separation_year VARCHAR(255) DEFAULT NULL, founders VARCHAR(255) DEFAULT NULL, members VARCHAR(255) DEFAULT NULL, presentation VARCHAR(255) DEFAULT NULL, INDEX IDX_2FB3D0EE56A273CC (origin_id), INDEX IDX_2FB3D0EEF92F3E70 (country_id), INDEX IDX_2FB3D0EE399BBB13 (music_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE56A273CC FOREIGN KEY (origin_id) REFERENCES origin (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE399BBB13 FOREIGN KEY (music_id) REFERENCES music (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE56A273CC');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEF92F3E70');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE399BBB13');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE music');
        $this->addSql('DROP TABLE origin');
        $this->addSql('DROP TABLE project');
    }
}
