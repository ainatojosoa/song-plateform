export interface  Country {
  id: number;
  group_name?: string;
  origin: string;
  country: string;
  start_year: number;
  separation_year?: number;
  founders?: string;
  members?: number;
  current_musical: string;
  presentation: string;
}
