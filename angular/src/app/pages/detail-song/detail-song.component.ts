import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detail-song',
  templateUrl: './detail-song.component.html',
  styleUrls: ['./detail-song.component.scss']
})
export class DetailSongComponent {
  dataDetail: any;
  constructor(private route: ActivatedRoute, private dataService: DataService, private location: Location) {
    this.getDetail(this.route.snapshot.params["id"]);
  }

  getDetail(id: number): void {
    console.log(id);

    this.dataService.getOneData(id)
      .subscribe({
        next: (data) => {
          console.log(data);

         this.dataDetail = data;
        },
        error: (e) => console.error(e)
      });
  }
  goBack() {
    this.location.back();
  }
  }
